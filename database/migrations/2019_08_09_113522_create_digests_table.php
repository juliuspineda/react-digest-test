<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDigestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('digests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('author');
            $table->text('title');
            $table->text('category');
            $table->text('summary');
            $table->text('doctrine');
            $table->text('facts');
            $table->text('issues_ratio');
            $table->text('dispositive');
            $table->text('note')->nullable();            
            $table->text('rating');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('digests');
    }
}
