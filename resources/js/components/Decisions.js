import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'

class Decisions extends Component {
	constructor(props) {
		super(props)

		this.state = {
			lists: []
		}
	}

	componentWillMount() {
		fetch('/api/decisions')
		.then((response) => response.json())
		.then((lists) => {
			console.log(lists)
			this.setState({
				lists: lists
			})
		})
	}

	render() {
		return (
			<div class="position-relative">
			      <section class="section section-lg pb-250 section-shaped">
			        <div class="separator separator-bottom separator-skew">
			          <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
			            <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
			          </svg>
			        </div>
			      </section>
			      <div className="container mx-auto pt-5">
			      		<div class="input-group mb-4">
	                <div class="input-group-prepend">
	                  <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
	                </div>
	                <input class="form-control" placeholder="Search" type="text"/>
	              </div>
			      </div>
    		<div className="row mt-3">
			{
				this.state.lists.map((list) => {
					return (
						<div className="container mx-auto">
							<div key={ list.id } className="col-12 mb-3">	
								<div className="card h-100">
									<div className="card-body">
										<h4 className="card-title"></h4>
										<p className="card-text"><strong>Title: </strong>{ list.title }</p>
										<p className="card-text"><strong>Author: </strong>{ list.author }</p>
										<p className="card-text"><strong>Category: </strong>{ list.category }</p>
									</div>
								</div>
							</div>
						</div>
					)
				})
			}
			</div>
				<div className="container mx-auto">
			      		<nav aria-label="Page navigation example">
                <ul class="pagination">
                  <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-left"></i></a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item active"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">4</a></li>
                  <li class="page-item"><a class="page-link" href="#">5</a></li>
                  <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-right"></i></a></li>
                </ul>
              </nav>
			      </div>

		    </div>
		)
	}
}

export default Decisions
