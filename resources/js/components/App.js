import React, { Component, Fragment } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Footer from './Footer'
import Header from './Header'

import Menu from './Menu'
import Decisions from './Decisions'

import '../assets/css/react.min.css'
import '../assets/css/main.css'

class App extends Component {

    render () {
        let MenuComponent = (props) => (<Menu {...props}/>)
        let DecisionsComponent = (props) => (<Decisions {...props}/>)
        return (
            <Fragment>
                <BrowserRouter>
                    <Header/>
                        <Switch>
    
                            <Route exact path="/" render={ MenuComponent }/>
                            <Route exact path="/decisions" render={ DecisionsComponent }/>

                        </Switch>
                </BrowserRouter>
                <Footer/>
            </Fragment>
            )
    }
}


ReactDOM.render(<App/>, document.getElementById('app'));