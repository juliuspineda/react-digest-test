import React, { Fragment } from 'react'

const Footer = () => {
	return(
		<Fragment>
        <footer class="footer has-cards">
          <div class="container">
            <hr/>
            <div class="row align-items-center justify-content-md-between">
              <div class="col-md-6">
                <div class="copyright">
                  &copy; Copyright <b>Digest 2019</b>. All Rights Reserved
                </div>
              </div>
              <div class="col-md-6">
                <ul class="nav nav-footer justify-content-end">
                  <li class="nav-item">
                    <a href="/" class="nav-link" target="_blank">Home</a>
                  </li>
                  <li class="nav-item">
                    <a href="/" class="nav-link" target="_blank">Feedback &#38; Suggestions</a>
                  </li>
                  <li class="nav-item">
                    <a href="/" class="nav-link" target="_blank">Privacy Policy</a>
                  </li>
                  <li class="nav-item">
                    <a href="/" class="nav-link" target="_blank">Terms of Use</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
    </Fragment>
	)
}

export default Footer