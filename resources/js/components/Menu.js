import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'

class MenuList extends Component {

	render() {
		
		return (
		<Fragment>
		<div class="position-relative">
	      <section class="section section-lg pb-250 section-shaped">
	        <div class="container py-lg-md d-flex">
	          <div class="col px-0">
	            <div class="row">
	              <div class="col-lg-4">
	                <h1 class="display-3  text-white">Search our library. It's free!</h1>
	                <p class="lead  text-white">We have the best collection of decisions and laws.</p>
	                <div class="form-group">
		              <div class="input-group mb-4">
		                <div class="input-group-prepend">
		                  <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
		                </div>
		                <input class="form-control" placeholder="Search" type="text"/>
		              </div>
		            </div>
	                <div class="btn-wrapper">
	                  <a href="/" class="btn btn-warning btn-icon mb-3 mb-sm-0">
	                    
	                    <span class="btn-outline-primary">Search</span>
	                  </a>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	      
	        <div class="separator separator-bottom separator-skew">
	          <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
	            <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
	          </svg>
	        </div>
	      </section>
   
       </div>
		<section class="section section-lg pt-lg-0 mt--200">
	      <div class="container">
	        <div class="row justify-content-center">
	          <div class="col-lg-12">
	            <div class="row row-grid">
	              <div class="col-lg-4">
	                <div class="card card-lift--hover shadow border-0">
	                  <div class="card-body py-5">
	                    <div class="icon icon-shape icon-shape-warning rounded-circle mb-4">
	                      <i class="fa fa-university" aria-hidden="true"></i>
	                    </div>
	                    <h6 class="text-warning text-uppercase">Case Digests</h6>
	                    <p class="description mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
	                    <div>
	                      <span class="badge badge-pill badge-warning">Lorem</span>
	                      <span class="badge badge-pill badge-warning">Lorem</span>
	                      <span class="badge badge-pill badge-warning">Lorem</span>
	                    </div>
	                    <a href="#" class="btn btn-warning mt-4">Read more</a>
	                  </div>
	                </div>
	              </div>
	              <div class="col-lg-4">
	                <div class="card card-lift--hover shadow border-0">
	                  <div class="card-body py-5">
	                    <div class="icon icon-shape icon-shape-warning rounded-circle mb-4">
	                      <i class="fa fa-university" aria-hidden="true"></i>
	                    </div>
	                    <h6 class="text-warning text-uppercase">Bar Exams</h6>
	                    <p class="description mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
	                    <div>
	                      <span class="badge badge-pill badge-warning">Lorem</span>
	                      <span class="badge badge-pill badge-warning">Lorem</span>
	                      <span class="badge badge-pill badge-warning">Lorem</span>
	                    </div>
	                    <a href="#" class="btn btn-warning mt-4">Read more</a>
	                  </div>
	                </div>
	              </div>
	              <div class="col-lg-4">
	                <div class="card card-lift--hover shadow border-0">
	                  <div class="card-body py-5">
	                    <div class="icon icon-shape icon-shape-warning rounded-circle mb-4">
	                      <i class="fa fa-university" aria-hidden="true"></i>
	                    </div>
	                    <h6 class="text-warning text-uppercase">Law Reviewers</h6>
	                    <p class="description mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
	                    <div>
	                      <span class="badge badge-pill badge-warning">Lorem</span>
	                      <span class="badge badge-pill badge-warning">Lorem</span>
	                      <span class="badge badge-pill badge-warning">Lorem</span>
	                    </div>
	                    <a href="#" class="btn btn-warning mt-4">Read more</a>
	                  </div>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
    	</section>
    	<section class="section section-lg">
		      <div class="container">
		        <div class="row row-grid align-items-center">
		          <div class="col-md-6 order-md-2">
		            <div class="rounded shadow-lg overflow-hidden transform-perspective-right">
		              <div id="carousel_example" class="carousel slide" data-ride="carousel">
		                <ol class="carousel-indicators">
		                  <li data-target="#carousel_example" data-slide-to="0" class="active"></li>
		                  <li data-target="#carousel_example" data-slide-to="1"></li>
		                </ol>
		                <div class="carousel-inner">
		                  <div class="carousel-item active">
		                    <img class="img-fluid" src="../images/supporting-2.jpg" alt="First slide"/>
		                  </div>
		                  <div class="carousel-item">
		                    <img class="img-fluid" src="../images/supporting-3.jpg" alt="Second slide"/>
		                  </div>
		                </div>
		                <a class="carousel-control-prev" href="#carousel_example" role="button" data-slide="prev">
		                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		                  <span class="sr-only">Previous</span>
		                </a>
		                <a class="carousel-control-next" href="#carousel_example" role="button" data-slide="next">
		                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
		                  <span class="sr-only">Next</span>
		                </a>
		              </div>
            </div>
		          </div>
		          <div class="col-md-6 order-md-1">
		            <div class="pr-md-5">
		              <div class="icon icon-lg icon-shape icon-shape-warning shadow rounded-circle mb-5">
		                <i class="fa fa-university" aria-hidden="true"></i>
		              </div>
		              <h3>Search for lawyers</h3>
		              <p>Look through our database of all 68,134 Philippine lawyers.</p>
		              <ul class="list-unstyled mt-5">
		                <li class="py-2">
		                  <div class="d-flex align-items-center">
		                    <div>
		                      <div class="badge badge-circle badge-warning mr-3">
		                        <i class="fa fa-university" aria-hidden="true"></i>
		                      </div>
		                    </div>
		                    <div>
		                      <h6 class="mb-0">Transparent Pricing</h6>
		                    </div>
		                  </div>
		                </li>
		                <li class="py-2">
		                  <div class="d-flex align-items-center">
		                    <div>
		                      <div class="badge badge-circle badge-warning mr-3">
		                        <i class="fa fa-university" aria-hidden="true"></i>
		                      </div>
		                    </div>
		                    <div>
		                      <h6 class="mb-0">Quality of Legal Service</h6>
		                    </div>
		                  </div>
		                </li>
		                <li class="py-2">
		                  <div class="d-flex align-items-center">
		                    <div>
		                      <div class="badge badge-circle badge-warning mr-3">
		                        <i class="fa fa-university" aria-hidden="true"></i>
		                      </div>
		                    </div>
		                    <div>
		                      <h6 class="mb-0">Convenient Location</h6>
		                    </div>
		                  </div>
		                </li>
		                <li class="py-2">
		                  <div class="d-flex align-items-center">
		                    <div>
		                      <div class="badge badge-circle badge-warning mr-3">
		                        <i class="fa fa-university" aria-hidden="true"></i>
		                      </div>
		                    </div>
		                    <div>
		                      <h6 class="mb-0">Specialization</h6>
		                    </div>
		                  </div>
		                </li>
		              </ul>
		            </div>
		          </div>
		        </div>
		      </div>
    </section>
		    <section class="pt-0">
		      <div class="container">
		        <div class="card bg-gradient-warning shadow-lg border-0">
		          <div class="p-5">
		            <div class="row align-items-center">
		              <div class="col-lg-8">
		                <h3 class="text-white">Digest</h3>
		                <p class="lead text-white mt-3">Digest is a legal technology platform committed to modernizing the Philippine legal system.</p>
		              </div>
		              <div class="col-lg-3 ml-lg-auto">
		                <a href="/" class="btn btn-lg btn-block btn-white">Contact Us</a>
		              </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </section>
    	</Fragment>
		)
	}

}

export default MenuList