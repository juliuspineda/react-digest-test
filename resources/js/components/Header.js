import React from 'react'
import { Link } from 'react-router-dom'

const Header = (props) => {

	return(
		<header class="header-global">
		    <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light headroom">
		      <div class="container">
		        <a class="navbar-brand mr-lg-5" href="/">
		          <img src="/" alt="Digest"/>
		        </a>
		        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
		          <span class="navbar-toggler-icon"></span>
		        </button>
		        <div class="navbar-collapse collapse" id="navbar_global">
		          <div class="navbar-collapse-header">
		            <div class="row">
		              <div class="col-6 collapse-brand">
		                <a href="/">
		                  <img src="/" alt="Digest"/>
		                </a>
		              </div>
		              <div class="col-6 collapse-close">
		                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
		                  <span></span>
		                  <span></span>
		                </button>
		              </div>
		            </div>
		          </div>
		          <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
		            <li class="nav-item">
		            	 <a href="/decisions" class="nav-link">
			                <i class="ni ni-collection d-lg-none"></i>
			                <span class="nav-link-inner--text">Decisions</span>
		              </a>
		            </li>
		            <li class="nav-item dropdown">
		              <a href="#" class="nav-link" data-toggle="dropdown" role="button">
		                <i class="ni ni-collection d-lg-none"></i>
		                <span class="nav-link-inner--text">Laws</span>
		              </a>
		              <div class="dropdown-menu">
		                <a href="#" class="dropdown-item">Constitutions</a>
		                <a href="#" class="dropdown-item">Statutes</a>
		                <a href="#" class="dropdown-item">Executive Issuances</a>
		                <a href="#" class="dropdown-item">Rules</a>
		              </div>
		            </li>
		            <li class="nav-item dropdown">
		              <a href="#" class="nav-link" data-toggle="dropdown" role="button">
		                <i class="ni ni-ui-04 d-lg-none"></i>
		                <span class="nav-link-inner--text">Resources</span>
		              </a>
		              <div class="dropdown-menu dropdown-menu-xl">
		                <div class="dropdown-menu-inner">
		                  <a href="#" class="media d-flex align-items-center">
		                    <div class="icon icon-shape bg-gradient-warning rounded-circle text-white">
		                      <i class="fa fa-book" aria-hidden="true"></i>
		                    </div>
		                    <div class="media-body ml-3">
		                      <h6 class="heading text-warning mb-md-1">Bar Exams</h6>
		                      <p class="description d-none d-md-inline-block mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		                    </div>
		                  </a>
		                  <a href="/" class="media d-flex align-items-center">
		                    <div class="icon icon-shape bg-gradient-warning rounded-circle text-white">
		                      <i class="fa fa-sticky-note" aria-hidden="true"></i>
		                    </div>
		                    <div class="media-body ml-3">
		                      <h6 class="heading text-warning mb-md-1">Reviewers</h6>
		                      <p class="description d-none d-md-inline-block mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		                    </div>
		                  </a>
		                  <a href="/" class="media d-flex align-items-center">
		                    <div class="icon icon-shape bg-gradient-warning rounded-circle text-white">
		                      <i class="fa fa-university" aria-hidden="true"></i>
		                    </div>
		                    <div class="media-body ml-3">
		                      <h5 class="heading text-warning mb-md-1">Legal Aid Offices</h5>
		                      <p class="description d-none d-md-inline-block mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		                    </div>
		                  </a>
		                  <a href="/" class="media d-flex align-items-center">
		                    <div class="icon icon-shape bg-gradient-warning rounded-circle text-white">
		                      <i class="fa fa-gavel" aria-hidden="true"></i>
		                    </div>
		                    <div class="media-body ml-3">
		                      <h5 class="heading text-warning mb-md-1">Court Directory</h5>
		                      <p class="description d-none d-md-inline-block mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		                    </div>
		                  </a>
		                  <a href="/" class="media d-flex align-items-center">
		                    <div class="icon icon-shape bg-gradient-warning rounded-circle text-white">
		                      <i class="fa fa-list" aria-hidden="true"></i>
		                    </div>
		                    <div class="media-body ml-3">
		                      <h5 class="heading text-warning mb-md-1">Law Dictionary</h5>
		                      <p class="description d-none d-md-inline-block mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		                    </div>
		                  </a>
		                </div>
		              </div>
		            </li>
		            <li class="nav-item">
		            	 <a href="#" class="nav-link">
			                <i class="ni ni-collection d-lg-none"></i>
			                <span class="nav-link-inner--text">Lawyers</span>
		              </a>
		            </li>
		            <li class="nav-item">
		            	 <a href="#" class="nav-link">
			                <i class="ni ni-collection d-lg-none"></i>
			                <span class="nav-link-inner--text">Contact Us</span>
		              </a>
		            </li>
		          </ul>
		          <ul class="navbar-nav align-items-lg-center ml-lg-auto">
		            <li class="nav-item">
		              <a class="nav-link nav-link-icon" href="#" target="_blank" data-toggle="tooltip" title="Like us on Facebook">
		                <i class="fa fa-facebook-square"></i>
		                <span class="nav-link-inner--text d-lg-none">Facebook</span>
		              </a>
		            </li>
		            <li class="nav-item">
		              <a class="nav-link nav-link-icon" href="#" target="_blank" data-toggle="tooltip" title="Follow us on Instagram">
		                <i class="fa fa-instagram"></i>
		                <span class="nav-link-inner--text d-lg-none">Instagram</span>
		              </a>
		            </li>
		            <li class="nav-item">
		              <a class="nav-link nav-link-icon" href="#" target="_blank" data-toggle="tooltip" title="Follow us on Twitter">
		                <i class="fa fa-twitter-square"></i>
		                <span class="nav-link-inner--text d-lg-none">Twitter</span>
		              </a>
		            </li>
		            <li class="nav-item">
		              <a class="nav-link nav-link-icon" href="#" target="_blank" data-toggle="tooltip" title="Linkedin">
		                <i class="fa fa-linkedin"></i>
		                <span class="nav-link-inner--text d-lg-none">Linkedin</span>
		              </a>
		            </li>
		            <li class="nav-item d-none d-lg-block ml-lg-4">
		              <a href="#" target="_blank" class="btn btn-neutral btn-icon" title="Login">
		                <span class="btn-inner--icon">
		                  <i class="fa fa-user-circle-o text-warning" aria-hidden="true"></i>
		                </span>
		                <span class="nav-link-inner--text text-warning">&nbsp;&nbsp;Login</span>
		              </a>
		            </li>
		          </ul>
		        </div>
		      </div>
		    </nav>
	  	</header>
	)
}

export default Header