<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    public function all() {
    	return User::where('user_role', 'customer')->get();
    }

    public function customer() {
    	return User::where('user_role', 'customer')->get();
    }

    public function create(Request $request) {
        return User::create([
        'name' => $request->name,
        'email' => $request->email,
        'password' => Hash::make($request->password),
        'user_role' => 'customer',
        'active' => 'Active'
    ]);
    }

    public function update(Request $request) {
    	$user = User::find($request->id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->user_role = $request->role;
        $user->active = $request->active;

        $user->save();

        return $user;
    }

    public function find($id) {
    	return User::find($id);
    }

    public function delete(Request $request) {
        return User::where('id', $request->id)->update(['active' => 'Not Active']);
    }
}
