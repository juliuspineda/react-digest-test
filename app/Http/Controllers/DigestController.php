<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Digest;

class DigestController extends Controller
{
    public function all() {
    	return Digest::limit(5)->get();
    }
}
